# benchmarks

## `rpc-amplification.R`

This benchmark tests how RPC between nodes having
non negligible latency will impact the final S3 request latency.

The idea is that multiple communications must be done over the network
between nodes of the cluster to answer an S3 request, and some of them
are even sequentials. Depending of the chosen algorithms and implementations,
the final S3 request latency will vary.

You need to install first R, R development headers, python3, pip3, wget, iproute2 and maybe some other dependencies.
Then run:

```
sudo ./rpc-amplification.R
```

you can test a different topology with:

```
MKNET_DC=./mknet/with-vdsl.yml sudo ./rpc-amplification.R
```

![Generated plot for RPC amplification](./png/rpc-amplification.png)

*This benchmark should be fully automated.*

## `rpc-complexity.R`

This benchmark validates that the time to complete RPC for a
given S3 request is stable when the number of node change.
This is not always the case: some distributed protocols send a quadratic number
of messages according to the number of nodes.

You need an access to [Grid5000](https://grid5000.fr).
Then import `jupyter/rpc-complexity.ipynb` in a [Grid5000 Jupyter Notebook](https://intranet.grid5000.fr/).

Download all the `.csv` files in this `csv/` folder.
And finally run:

```
./rpc-complexity.R
```

![Generated plot for RPC complexity](./png/rpc-complexity.png)

*This benchmark is not fully automated.*
