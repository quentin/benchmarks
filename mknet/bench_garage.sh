#!/bin/bash

echo "10 sec to start"
sleep 10

CONFIG_NODE_FPATH=$(find /tmp/garage-testnet/ -maxdepth 3 -name garage.toml|head -n 1)

garage -c ${CONFIG_NODE_FPATH} key new --name bench
for i in $(seq 1 10); do 
  garage -c ${CONFIG_NODE_FPATH} bucket create bench$i
  garage -c ${CONFIG_NODE_FPATH} bucket allow --read --write bench$i --key bench
done

openssl req \
    -new \
    -x509 \
    -keyout /tmp/garage.key \
    -out /tmp/garage.crt \
    -nodes  \
    -subj "/C=XX/ST=XX/L=XX/O=XX/OU=XX/CN=localhost/emailAddress=X@X.XX" \
    -addext "subjectAltName = DNS:localhost, IP:127.0.0.1"

cat /tmp/garage.key /tmp/garage.crt > /tmp/garage.pem

socat "openssl-listen:4443,\
reuseaddr,\
fork,\
verify=0,\
cert=/tmp/garage.pem" tcp4-connect:localhost:3900 &

sleep 1

export SSL=1
export SSL_INSECURE=1
export REGION=garage
export ENDPOINT=localhost:4443
export AWS_ACCESS_KEY_ID=$(garage -c ${CONFIG_NODE_FPATH} key info bench|grep 'Key ID:'|grep -Po 'GK[a-f0-9]+$')
export AWS_SECRET_ACCESS_KEY=$(garage -c ${CONFIG_NODE_FPATH} key info bench|grep 'Secret key:'|grep -Po '[a-f0-9]+$')

s3lat | tee 50ms.garage.csv
