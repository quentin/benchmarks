#!/bin/bash

echo "sleep 3 min to wait for minio bootstrap"
sleep 180

export ENDPOINT=localhost:9000
export AWS_ACCESS_KEY_ID=minioadmin
export AWS_SECRET_ACCESS_KEY=minioadmin

mc alias set minio-bench http://$ENDPOINT $AWS_ACCESS_KEY_ID $AWS_SECRET_ACCESS_KEY
for i in $(seq 1 10); do 
  mc mb minio-bench/bench$i
done

s3lat | tee 50ms.minio.csv
