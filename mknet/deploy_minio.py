#!/usr/bin/env python3
import json, os, sys, time, pathlib, socket, shutil

STORAGE_PATH = os.path.join(os.getcwd(), '.minio-testnet')
HOSTS_PATH = os.path.join(STORAGE_PATH, 'hosts.txt')
UNIX_SOCK = os.path.join(STORAGE_PATH, 'deploy.sock')
DATA_PATH = lambda nid: os.path.join(STORAGE_PATH, 'data'+str(nid))

def main():
    if int(os.environ['ID']) == 1: leader()
    else: follower()

def leader():
    shutil.rmtree(STORAGE_PATH, ignore_errors=True)
    os.makedirs(STORAGE_PATH)
    print(STORAGE_PATH)

    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.bind(UNIX_SOCK)
    sock.listen()

    n_serv = int(os.environ['SERVER_COUNT'])
    fl = [ co for co, addr in [ sock.accept() for i in range(n_serv - 1) ]]

    identities = [ json.loads(co.makefile().readline()) for co in fl ] + [ { "ip": os.environ['IP'], "path": make_data() } ]
    print(f"ident: {identities}")
    msg = f"{json.dumps(identities)}\n".encode()
    [ co.send(msg) for co in fl ]

    run_minio(identities)

def follower():
    co = None
    while True:
        time.sleep(1)
        try:
            sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
            sock.connect(UNIX_SOCK)
            co = sock.makefile()
            break
        except Exception as err:
            print('conn failed, wait,', err)
    my_identity = json.dumps({ "ip": os.environ['IP'], "path": make_data() })
    sock.send(f"{my_identity}\n".encode())
    identities = json.loads(co.readline())

    run_minio(identities)

def make_data():
    data_path = DATA_PATH(os.environ['ID'])
    os.makedirs(data_path)
    return data_path

def run_minio(identities):
    cmd = f"minio server --console-address ':9001' --address ':9000'"
    for ident in identities:
        cmd += f" http://[{ident['ip']}]:9000{ident['path']}"
    cmd += f" > {os.path.join(STORAGE_PATH, 'minio'+os.environ['ID']+'.log')} 2>&1"
    print("launch: ", cmd)
    os.system(cmd)

__name__ == '__main__' and main()
