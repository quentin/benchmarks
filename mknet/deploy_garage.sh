#!/bin/bash

set -euo pipefail
IFS=$'\n\t'

GARAGE_PATH=garage
STORAGE_PATH=/tmp/garage-testnet
export RPC_SECRET=3e9abff5f9e480afbadb46a77b7a26fe0e404258f0dc3fd5386b0ba8e0ad2fba

if [ -z "$ZONE" ]; then
	NODE_STORAGE_PATH=${STORAGE_PATH}/${HOST}
else
	NODE_STORAGE_PATH=${STORAGE_PATH}/${ZONE}/${HOST}
fi
BOOTSTRAP_FILE=${STORAGE_PATH}/bootstrap_peer
export GARAGE_CONFIG_FILE=${NODE_STORAGE_PATH}/garage.toml 


mkdir -p ${NODE_STORAGE_PATH}
cd ${NODE_STORAGE_PATH}
rm ${BOOTSTRAP_FILE} 2>/dev/null || true

cat > ${GARAGE_CONFIG_FILE} << EOF
metadata_dir = "${NODE_STORAGE_PATH}/meta"
data_dir = "${NODE_STORAGE_PATH}/data"

replication_mode = "3"

rpc_bind_addr = "[::]:3901"
rpc_public_addr = "[${IP}]:3901"
rpc_secret = "${RPC_SECRET}"

bootstrap_peers=[]

[s3_api]
s3_region = "garage"
api_bind_addr = "[::]:3900"
root_domain = ".s3.garage"

[s3_web]
bind_addr = "[::]:3902"
root_domain = ".web.garage"
index = "index.html"
EOF

RUST_LOG=garage=debug ${GARAGE_PATH} server 2>> ${NODE_STORAGE_PATH}/logs & disown
sleep 2

CONFIG_NODE_FPATH=$(find /tmp/garage-testnet/ -maxdepth 3 -name garage.toml|head -n 1)

SELF_ID=$(${GARAGE_PATH} node id 2>/dev/null)
SHORT_ID=$(echo ${SELF_ID} | cut -c-64)

${GARAGE_PATH} -c ${CONFIG_NODE_FPATH} node connect ${SELF_ID}
${GARAGE_PATH} -c ${CONFIG_NODE_FPATH} layout assign ${SHORT_ID} -z ${ZONE:-unzonned-${HOST}} -c 1 -t ${HOST}

if [ ${CONFIG_NODE_FPATH} == ${GARAGE_CONFIG_FILE} ]; then
	sleep 2
	${GARAGE_PATH} layout show
	${GARAGE_PATH} layout apply --version 1
fi
